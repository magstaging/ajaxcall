# README #

Display a button click me on every page of the site. When clicking the button, a post ajax call is performed.
The response is a simple json message 

### How do I get set up? ###

1. clone the repository
2. create folder app/code/Mbs/AjaxCall when located at the root of the Magento site
3. copy the content of this repository within the folder
4. install the module php bin/magento setup:upgrade

