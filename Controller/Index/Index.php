<?php

namespace Mbs\AjaxCall\Controller\Index;

use Magento\Framework\App\Action\Context;
use Magento\Framework\App\Action\HttpPostActionInterface;
use Magento\Framework\Data\Form\FormKey\Validator;

class Index extends \Magento\Framework\App\Action\Action implements HttpPostActionInterface
{
    /**
     * @var \Magento\Framework\Controller\ResultFactory
     */
    private $pageFactory;
    /**
     * @var Validator
     */
    private $formKeyValidator;

    public function __construct(
        Context $context,
        \Magento\Framework\Controller\ResultFactory $pageFactory,
        Validator $formKeyValidator
    ) {
        parent::__construct($context);
        $this->pageFactory = $pageFactory;
        $this->formKeyValidator = $formKeyValidator;
    }

    /**
     * @inheritDoc
     */
    public function execute()
    {
        $resultRedirect = $this->resultRedirectFactory->create();
        if (!$this->formKeyValidator->validate($this->getRequest())) {
            return $resultRedirect->setRefererUrl();
        }

        $productIds = $this->getRequest()->getParam('product_ids');

        $result = [
            'success' => true,
            'message' => 'I have been clicked by ajax'
        ];

        $this->getResponse()->representJson(
            $this->_objectManager->get(\Magento\Framework\Json\Helper\Data::class)->jsonEncode($result)
        );
    }
}
